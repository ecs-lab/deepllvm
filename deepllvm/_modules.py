''' Machine learning models and basic blocks.

This file contains torch.nn.Module instances to implement basic blocks and 
machine learning models.
'''

import torch
import torch.nn as nn


# -----------------------------------------------------------------------------
# BASIC BLOCKS
# -----------------------------------------------------------------------------
class BasicBlockConvolutional(nn.Module):
    '''Convolutional basic block to convolve and pool a sequence of tokens.

    A sequential module made up by a 1D convolution layer and a max-pooling
    layer with programmable dropout.
    '''
    def __init__(
         self,
         n_channels_in,
         n_channels_out,
         kernel_size,
         kernel_stride,
         kernel_padding,
         pooling_size_out,
         dropout):
        '''Convolutional basic block constructor.
        '''
        super().__init__()

        # 1D convolution layer.
        self._conv1d_layer = nn.Conv1d(
            in_channels=n_channels_in,
            out_channels=n_channels_out,
            kernel_size=kernel_size,
            stride=kernel_stride,
            padding=kernel_padding
        )

        # Pooling layer.
        self._pooling_layer = nn.AdaptiveMaxPool1d(
            output_size=pooling_size_out
        )

        # Dropout layer.
        self._dropout_layer = nn.Dropout(
            p=dropout
        )

    def forward(
         self, 
         x):
        '''Compute module output.
        '''
        x = torch.relu(self._conv1d_layer(x))
        x = self._pooling_layer(x)
        x = self._dropout_layer(x)

        return x


class BasicBlockLinear(nn.Module):
    '''Linear basic block to group a sequence of fully-connected layers.

    A sequential module made up by a stack of fully connected layers with
    customizable layers size and dropout.
    '''
    def __init__(
         self,
         n_features_in,
         layer_sizes,
         activations):
        '''Linear basic block constructor.
        '''
        super().__init__()

        # Check that at least two layer sizes are provided.
        if len(layer_sizes) < 1:
            raise ValueError('Provide at least the size of one layer.')

        # Check that the correct number of activation functions are provided.
        if len(activations) != len(layer_sizes):
            raise ValueError(
                'The number of activation functions and the number of '
                'fully-connected layers must match.'
            )

        # Register fully-connected layers and dropouts.
        self._layers = nn.ModuleList()
        for i in range(len(layer_sizes)):
            if i == 0:
                layer = nn.Linear(
                    in_features=n_features_in,
                    out_features=layer_sizes[i]
                )
            else:
                layer = nn.Linear(
                    in_features=layer_sizes[i-1],
                    out_features=layer_sizes[i]
                )
            self._layers.append(layer)

        # Register activation functions.
        self._activations = activations 

    def forward(
         self,
         x):
        '''Compute module output.
        '''
        bundle = zip(
            self._layers,
            self._activations
        )
        for layer, activation in bundle:
            x = layer(x)
            if activation is not None:
                x = activation(x)

        return x


# -----------------------------------------------------------------------------
# MACHINE LEARNING MODELS
# -----------------------------------------------------------------------------
class CNN(nn.Module):
    '''A deep learning model for code mapping based on 1D convolution layers.
    '''
    class Parameters:
        '''Helper class to initialize CNN source code mappers.
        '''
        def __init__(
             self,
             embedding_dim=128,
             conv_n_filters=32,
             conv_size=9,
             conv_stride=1,
             conv_padding=0,
             pool_n_out=1,
             pool_dropout=0.2,
             aux_sizes=(32,),
             aux_activations=(torch.relu,),
             mlp_sizes=(256, 2),
             mlp_activations=(torch.relu, torch.sigmoid)):
            self.embedding_dim = embedding_dim
            self.conv_n_filters = conv_n_filters
            self.conv_size = conv_size
            self.conv_stride = conv_stride
            self.conv_padding = conv_padding
            self.pool_n_out = pool_n_out
            self.pool_dropout = pool_dropout
            self.aux_sizes = aux_sizes
            self.aux_activations = aux_activations
            self.mlp_sizes = mlp_sizes
            self.mlp_activations = mlp_activations


    def __init__(
         self,
         configuration,
         vocabulary_size,
         auxiliary_size,
         mute_auxiliaries=False,
         preprocess_auxiliaries=False):
        super().__init__()

        # Register network parameters and auxiliary input control.
        self._cfg = configuration
        self._mute_auxiliaries = mute_auxiliaries
        self._preprocess_auxiliaries = preprocess_auxiliaries

        # Embedding layer.
        self._embedding = nn.Embedding(
            vocabulary_size,
            self._cfg.embedding_dim, 
            padding_idx=0
        )

        # Convolution-based source-code features extraction.
        self._conv_basic_block = BasicBlockConvolutional(
            self._cfg.embedding_dim,
            self._cfg.conv_n_filters,
            self._cfg.conv_size,
            self._cfg.conv_stride,
            self._cfg.conv_padding,
            self._cfg.pool_n_out,
            self._cfg.pool_dropout
        )
        n_language_features = self._cfg.conv_n_filters * self._cfg.pool_n_out

        # Auxiliary inputs pre-processing.
        if self._mute_auxiliaries:
            self._aux_basic_block = None
            n_auxiliaries_features = 0
        else:
            if self._preprocess_auxiliaries:
                self._aux_basic_block = BasicBlockLinear(
                    auxiliary_size,
                    self._cfg.aux_sizes,
                    self._cfg.aux_activations
                )
                n_auxiliaries_features = self._cfg.aux_sizes[-1]
            else:
                self._aux_basic_block = None
                n_auxiliaries_features = auxiliary_size

        # Batch normalization layer.
        n_linear_features = n_language_features + n_auxiliaries_features
        self._batch_norm = nn.BatchNorm1d(n_linear_features)

        # Multi-layer perceptron classification layer.
        self._mlp_basic_block = BasicBlockLinear(
            n_linear_features,
            self._cfg.mlp_sizes,
            self._cfg.mlp_activations
        )

    @property
    def cfg(self):
        '''Get the hyper-parameters of the network.
        '''
        return self._cfg

    def forward(
         self,
         sequences,
         auxiliaries):
        # Embedding layer : <?, L> -> <?, L, H>
        x_sequences = self._embedding(sequences)
        
        # Transposition : <?, L, H> -> <?, H, L>
        x_sequences = torch.transpose(
            x_sequences, 
            1, 
            2
        )
        
        # Convolution language modelling : <?, H, L> -> <?, KF, P>
        x_sequences = self._conv_basic_block(x_sequences)

        # Flattening : <?, KF, P> -> <?, KF*P>
        x_sequences = torch.flatten(
            x_sequences, 
            1
        )

        # Auxiliaries pre-processing : <?, AUX> -> <?, Z>
        if self._mute_auxiliaries:
            # If auxiliaries are muted, forward language modelling features to
            # the batch normalization layer.
            x = x_sequences
        else:
            # Auxiliaries pre-processing : <?, AUX> -> <?, Z>
            if self._preprocess_auxiliaries:
                x_auxiliaries = self._aux_basic_block(auxiliaries)
            else:
                x_auxiliaries = auxiliaries

            # Concatenate auxiliaries : <?, KF*P> -> <?, KF*P+Z>
            x = torch.cat(
                (
                    x_sequences,
                    x_auxiliaries
                ),
                1
            )

        # If the model is being trained, by-pass the batch normalization layer
        # if the batch has a single sample.
        if not self.training or x.shape[0] > 1:
            # <?, KF*P+Z> -> <?, KF*P+Z>
            x = self._batch_norm(x)
        
        # <?, KF*P+Z> -> <?, O>
        x = self._mlp_basic_block(x)

        return x


# -----------------------------------------------------------------------------
# SIAMESE MODELS
# -----------------------------------------------------------------------------
class ContrastiveLoss(nn.Module):
    def __init__(
         self,
         margin):
        super().__init__()

        self._margin = margin
        self._distance_fn = nn.PairwiseDistance(p=2)

    def forward(
         self,
         y1,
         y2,
         label):
        distance = self._distance_fn(
            y1,
            y2
        )
        marginal = torch.clamp(
            self._margin - distance,
            min=0.0
        )
        loss_0 = torch.square(distance)
        loss_1 = torch.square(marginal)
        loss = torch.mean((1 - label) * loss_0 + label * loss_1)

        return loss


class Siamese(nn.Module):
    def __init__(
         self,
         core):
        super().__init__()

        self._core = core

    @property
    def core(self):
        '''Access the machine learning model inside the siamese envelope.
        '''
        return self._core

    def compute_centroids(
         self,
         dataloader,
         n_classes):
        '''Run inference on input data with siamese core to compute centroids.
        '''
        # Discover the device where the model is running.
        device = next(self.parameters()).device

        self.eval()

        outputs = []
        labels = []
        with torch.no_grad():
            centroids = None

            # Run inference on input data.
            for samples in dataloader:
                # Unpack batch data.
                batch_sequences = samples[0].to(device)
                batch_auxiliaries = samples[1].to(device)
                batch_labels = samples[2].to(device)

                # Check model prediction and update centroids.
                batch_outputs = self._core(
                    batch_sequences,
                    batch_auxiliaries
                )
                outputs.append(batch_outputs)
                labels.append(batch_labels)
            outputs = torch.vstack(outputs)
            labels = torch.hstack(labels)

            # Compute centroids.
            centroids = torch.zeros(
                (
                    n_classes,
                    outputs.shape[1]
                ),
                dtype=torch.float
            )
            if device is not None:
                centroids = centroids.to(device)
            for i in range(n_classes):
                class_outputs = torch.index_select(
                    outputs,
                    0,
                    torch.nonzero(labels == i).squeeze(dim=1)
                )
                centroids[i] = torch.mean(
                    class_outputs,
                    axis=0
                )

        return centroids

    def forward(
         self,
         sequences_1,
         auxiliaries_1,
         sequences_2,
         auxiliaries_2):
        x1 = self._core(
            sequences_1,
            auxiliaries_1
        )
        x2 = self._core(
            sequences_2,
            auxiliaries_2
        )

        return x1, x2
