'''Utilities to train/test models and to collect results.
'''

import numpy
import sklearn.metrics
import torch


# -----------------------------------------------------------------------------
# TRAIN/TEST MODELS
# -----------------------------------------------------------------------------
def model_train(
     model,
     dataloader,
     loss_fn,
     optimizer,
     device=None):
    '''Train a model for a single epoch on the selected device.
    '''
    model.train()

    losses = []
    for samples in dataloader:
        # Unpack samples data.
        batch_sequences = samples[0]
        batch_auxiliaries = samples[1]
        batch_labels = samples[2]

        # Move data to the target device.
        if device is not None:
            batch_sequences = batch_sequences.to(device)
            batch_auxiliaries = batch_auxiliaries.to(device)
            batch_labels = batch_labels.to(device)

        # Compute loss and update model weights.
        optimizer.zero_grad()
        outputs = model(
            batch_sequences,
            batch_auxiliaries
        )
        loss = loss_fn(
            outputs,
            batch_labels
        )
        losses.append(loss.item())
        loss.backward()
        optimizer.step()

    return numpy.mean(losses)


def model_test(
     model,
     dataloader,
     loss_fn,
     device=None):
    '''Test the model on the dataloader provided as input.
    '''
    model.eval()

    losses = []
    predictions = []
    labels = []
    with torch.no_grad():
        for samples in dataloader:
            # Unpack samples data.
            batch_sequences = samples[0]
            batch_auxiliaries = samples[1]
            batch_labels = samples[2]

            # Move data to the target device.
            if device is not None:
                batch_sequences = batch_sequences.to(device)
                batch_auxiliaries = batch_auxiliaries.to(device)
                batch_labels = batch_labels.to(device)

            # Compute loss and accuracy on the provided data.
            outputs = model(
                batch_sequences,
                batch_auxiliaries
            )
            loss = loss_fn(
                outputs,
                batch_labels
            )
            losses.append(loss.item())
            if outputs.shape[1] > 1:
                predictions.append(torch.argmax(
                    outputs,
                    axis=1
                ).cpu().numpy())
            else:
                predictions.append(torch.round(outputs.cpu().numpy()))
            labels.append(batch_labels.cpu().numpy())

    # Convert batch results to numpy data arrays.
    predictions = numpy.hstack(predictions)
    labels = numpy.hstack(labels)

    return numpy.mean(losses), predictions, labels


# -----------------------------------------------------------------------------
# SIAMESE
# -----------------------------------------------------------------------------
def siamese_model_train(
     model,
     dataloader,
     loss_fn,
     optimizer,
     device=None):
    '''Train a siamese model for a single epoch on the selected device.
    '''
    model.train()

    losses = []
    for samples in dataloader:
        # Unpack batch data.
        batch_sequences_1 = samples[0]
        batch_auxiliaries_1 = samples[1]
        batch_sequences_2 = samples[2]
        batch_auxiliaries_2 = samples[3]
        batch_labels = samples[4]

        # Move data to the target device.
        if device is not None:
            batch_sequences_1 = batch_sequences_1.to(device)
            batch_auxiliaries_1 = batch_auxiliaries_1.to(device)
            batch_sequences_2 = batch_sequences_2.to(device)
            batch_auxiliaries_2 = batch_auxiliaries_2.to(device)
            batch_labels = batch_labels.to(device)

        # Compute loss and update model weights.
        optimizer.zero_grad()
        outputs_1, outputs_2 = model(
            batch_sequences_1,
            batch_auxiliaries_1,
            batch_sequences_2,
            batch_auxiliaries_2
        )
        loss = loss_fn(
            outputs_1,
            outputs_2,
            batch_labels
        )
        losses.append(loss.item())
        loss.backward()
        optimizer.step()

    return numpy.mean(losses)


def siamese_model_test(
     model,
     dataloader,
     centroids,
     device=None):
    '''Test the siamese model on the dataloader provided as input.
    '''
    model.eval()

    outputs = []
    predictions = []
    labels = []
    with torch.no_grad():
        for samples in dataloader:
            # Unpack batch data.
            batch_sequences = samples[0]
            batch_auxiliaries = samples[1]
            batch_labels = samples[2]

            # Move data to the target device.
            if device is not None:
                batch_sequences = batch_sequences.to(device)
                batch_auxiliaries = batch_auxiliaries.to(device)
                batch_labels = batch_labels.to(device)

            # Compare network inference with centroid posisitons.
            batch_outputs = model.core(
                batch_sequences,
                batch_auxiliaries
            )
            distances = torch.cdist(
                batch_outputs,
                centroids,
                p=2
            )
            outputs.append(batch_outputs.cpu().numpy())
            predictions.append(torch.argmin(
                distances,
                dim=1
            ).cpu().numpy())
            labels.append(batch_labels.cpu().numpy())

    # Convert batch results to numpy data arrays.
    outputs = numpy.vstack(outputs)
    predictions = numpy.hstack(predictions)
    labels = numpy.hstack(labels)

    return outputs, predictions, labels


# -----------------------------------------------------------------------------
# COLLECT RESULTS
# -----------------------------------------------------------------------------
class ExperimentResult:
    '''Summarize the results of the train/test of a model.
    '''
    @classmethod
    def merge(
         cls,
         results):
        '''Merge the outcome of a list of experiments.
        '''
        # Pack results data together.
        samples = []
        predictions = []
        labels = []
        meta = {}
        for i, r in enumerate(results):
            samples.append(r.samples)
            predictions.append(r.outcome[0])
            labels.append(r.outcome[1])
            meta[i] = r.meta

        # Convert data to numpy array.
        samples = numpy.hstack(samples)
        predictions = numpy.hstack(predictions)
        labels = numpy.hstack(labels)

        # Create overall result data structure.
        result = cls(
            samples,
            predictions,
            labels,
            meta=meta
        )

        return result

    def __init__(
         self,
         samples,
         predictions,
         labels,
         meta=None):
        '''Build the object from the results of an experiments.
        '''
        self._samples = samples
        self._predictions = predictions
        self._labels = labels
        if meta is None:
            self._meta = {}
        else:
            self._meta = meta

    @property
    def samples(self):
        '''Get the index of samples in the dataset used to test the model.
        '''
        return self._samples

    @property
    def outcome(self):
        '''Return the expected class values and the actual model predictions.
        '''
        return self._labels, self._predictions

    @property
    def meta(self):
        '''Return experiment result meta-data.
        '''
        return self._meta

    @property
    def accuracy(self):
        '''Helper method to compute the experiment accuracy score.
        '''
        score = sklearn.metrics.accuracy_score(
            self._labels,
            self._predictions
        )

        return score

    @property
    def mcc(self):
        '''Helper method to compute the experiment MCC score.
        '''
        score = sklearn.metrics.matthews_corrcoef(
            self._labels,
            self._predictions
        )

        return score
