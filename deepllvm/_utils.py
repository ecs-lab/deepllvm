import itertools
import sklearn.preprocessing
import subprocess
import torch


# -----------------------------------------------------------------------------
# PRE-PROCESSING
# -----------------------------------------------------------------------------
def compile_cl_to_llvmir(
     src_path, 
     out_path, 
     flags=None,
     clang_exe='clang'):
    '''Compile the input OpenCL source code to LLVM-IR using Clang.
    '''
    # Build compiler command line.
    cmdline = [
        clang_exe,
        '-S',
        '-emit-llvm',
        '-o',
        str(out_path)
    ]
    flags = flags or []
    for f in flags:
        cmdline.append(flag)
    cmdline.append('-Xclang')
    cmdline.append('-finclude-default-header')
    cmdline.append('-cl-std=CL2.0')
    cmdline.append(str(src_path))

    # Run clang.
    ret = subprocess.run(
        cmdline,
        stderr=subprocess.PIPE
    )
    if ret.returncode != 0:
        raise RuntimeError(ret.stderr.decode('utf-8'))


def normalize_samples(
     samples,
     test_samples=None):
    '''Normalize auxiliary inputs using a battery of transformers.

    The normalization procedure selectively applies a power transformation, a 
    standard transformation and a min-max scaler.
    '''
    # Instanciate scalers and transformers.
    engine_power = sklearn.preprocessing.PowerTransformer()
    engine_standard = sklearn.preprocessing.StandardScaler()
    engine_minmax = sklearn.preprocessing.MinMaxScaler(feature_range=(-1, 1))

    # Infer the scalers and transformers parameters from the train data.
    samples = engine_power.fit_transform(samples)
    samples = engine_standard.fit_transform(samples)
    samples = engine_minmax.fit_transform(samples)

    # Apply learnt transformations to test data, if available.
    if test_samples is not None:
        test_samples = engine_power.transform(test_samples)
        test_samples = engine_standard.transform(test_samples)
        test_samples = engine_minmax.transform(test_samples)

    return samples, test_samples


def make_siamese_dataset(dataset):
    '''Build a siamese dataset from a regular tensor dataset.
    '''
    sequences_1 = []
    sequences_2 = []
    auxiliaries_1 = []
    auxiliaries_2 = []
    labels = []

    # Compute pairs of dataset samples.
    pairs = itertools.combinations(
        dataset,
        2
    )
    for p in pairs:
        sequences_1.append(p[0][0])
        sequences_2.append(p[1][0])
        auxiliaries_1.append(p[0][1])
        auxiliaries_2.append(p[1][1])
        if p[0][2].item() == p[1][2].item():
            labels.append(0)
        else:
            labels.append(1)

    sequences_1 = torch.stack(sequences_1)
    sequences_2 = torch.stack(sequences_2)
    auxiliaries_1 = torch.stack(auxiliaries_1)
    auxiliaries_2 = torch.stack(auxiliaries_2)
    labels = torch.tensor(labels)

    siamese_dataset = torch.utils.data.TensorDataset(
        sequences_1,
        auxiliaries_1,
        sequences_2,
        auxiliaries_2,
        labels
    )

    return siamese_dataset
