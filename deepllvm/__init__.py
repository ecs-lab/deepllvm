__version__ = '1.0'


from ._experiments import model_train
from ._experiments import model_test
from ._experiments import siamese_model_train
from ._experiments import siamese_model_test
from ._experiments import ExperimentResult

from ._modules import BasicBlockConvolutional
from ._modules import BasicBlockLinear
from ._modules import ContrastiveLoss
from ._modules import CNN
from ._modules import Siamese

from ._tokenizers import LLVMTokenizer
from ._tokenizers import OpenCLTokenizer
from ._tokenizers import OPENCL_TOKENS

from ._utils import compile_cl_to_llvmir
from ._utils import make_siamese_dataset
from ._utils import normalize_samples
