import abc
import re


class Tokenizer(abc.ABC):
    '''Abstract representation on an engine to tokenize source code.
    '''
    def __init__(
         self,
         **kwargs):
        '''Initialize the tokenizer.
        '''
        pass

    @abc.abstractmethod
    def tokenize(
         self,
         kernel,
         **kwargs):
        '''Tokenize the kernel provided as input.
        '''
        pass


OPENCL_TOKENS = {
    '  ', '__assert', '__attribute', '__builtin_astype', '__clc_fabs', 
    '__clc_fma', '__constant', '__global', '__inline', '__kernel', '__local', 
    '__private', '__read_only', '__read_write', '__write_only', '*/', '/*', 
    '//', 'abs', 'alignas', 'alignof', 'atomic_add', 'auto', 'barrier', 'bool', 
    'break', 'case', 'char', 'clamp', 'complex', 'const', 'constant', 
    'continue', 'default', 'define', 'defined', 'do', 'double', 'elif', 'else', 
    'endif', 'enum', 'error', 'event_t', 'extern', 'fabs', 'false', 'float', 
    'for', 'get_global_id', 'get_global_size', 'get_local_id', 'get_local_size', 
    'get_num_groups', 'global', 'goto', 'half', 'if', 'ifdef', 'ifndef', 
    'image1d_array_t', 'image1d_buffer_t', 'image1d_t', 'image2d_array_t', 
    'image2d_t', 'image3d_t', 'imaginary', 'include', 'inline', 'int', 'into', 
    'kernel', 'line', 'local', 'long', 'noreturn', 'pragma', 'private', 'quad',
    'read_only', 'read_write', 'register', 'restrict', 'return', 'sampler_t', 
    'short', 'shuffle', 'signed', 'size_t', 'sizeof',  'sqrt', 'static',
    'struct', 'switch', 'true', 'typedef', 'u32', 'uchar', 'uint', 'ulong',
    'undef', 'union', 'unsigned', 'void', 'volatile', 'while', 'wide',
    'write_only'
}


class OpenCLTokenizer(Tokenizer):
    '''Engine to tokenize an OpenCL source code.
    '''
    def __init__(
         self,
         **kwargs):
        '''Initialize the OpenCL tokenizer.
        '''
        super().__init__(**kwargs)

        # Build a lookup table for speeding up retrieval of all tokens in the
        # frequent set starting with a certain character.
        self._lookup = {}
        for token in OPENCL_TOKENS:
            if len(token) > 1:
                if token[0] not in self._lookup:
                    self._lookup[token[0]] = [token]
                else:
                    self._lookup[token[0]].append(token)

    def tokenize(
         self,
         kernel,
         **kwargs):
        '''Tokenize the kernel provided as input.
        '''
        tokens = []
        i = 0
        j = 2
        while i < len(kernel):
            if kernel[i] in self._lookup:
                found_any = any(
                    x.startswith(kernel[i:j])
                    for x in self._lookup[kernel[i]]
                )
                if j <= len(kernel) and found_any:
                    j += 1
                else:
                    while j > i + 1:
                        found_any = any(
                            x == kernel[i:j]
                            for x in self._lookup[kernel[i]]
                        )
                        if found_any:
                            tokens.append(kernel[i:j])
                            i = j
                            j = j + 2
                            break
                        else:
                            j -= 1
                    else:
                        tokens.append(kernel[i])
                        i = i + 1
                        j = j + 2
            else:
                tokens.append(kernel[i])
                i = i + 1
                j = j + 2

        return tokens


class LLVMTokenizer(Tokenizer):
    '''Engine to tokenize an LLVM-IR source code.
    '''
    @classmethod
    def _line_remove_comments(
         cls,
         lines):
        filtered = []
        for line in lines:
            if ';' in line:
                position = line.find(';')
                filtered.append(line[:position])
            else:
                filtered.append(line)

        return filtered

    @classmethod
    def _line_remove_multi_spaces(
         cls,
         lines):
        filtered = []
        for line in lines:
            filtered.append(
                re.sub(
                    r'\s\s+',
                    ' ',
                    line
                )
            )

        return filtered

    @classmethod
    def _line_remove_empty_string(
         cls,
         lines):
        filtered = []
        for line in lines:
            if line:
                filtered.append(line)

        return filtered

    @classmethod
    def _line_extract_kernel_body(
         cls,
         lines):
        filtered = []
        inside_kernel = False
        for line in lines:
            if '{' in line and '}' not in line:
                inside_kernel = True
            elif '{' not in line and '}' in line:
                inside_kernel = False
            else:
                if inside_kernel:
                    filtered.append(line)

        return filtered

    @classmethod
    def _line_expand_symbol(
         cls,
         lines,
         symbol):
        filtered = []
        for line in lines:
            line = re.sub(
                f'\\{symbol}',
                f' {symbol} ',
                line
            )
            filtered.append(line)

        return filtered   

    @classmethod
    def _line_convert_type_vector(
         cls,
         lines):
        filtered = []
        for line in lines:
            matches = re.finditer(
                r'\<(\d+) x ([a-z]+[0-9]*\*?)\>', 
                line
            )
            for match in matches:
                line = line.replace(
                    match.group(),
                    '_{}_{}'.format(
                        match.group(2), 
                        match.group(1)
                    )
                )
            filtered.append(line)

        return filtered

    @classmethod
    def _line_convert_type_array(
         cls,
         lines):
        filtered = []
        for line in lines:
            matches = re.finditer(
                r'(\[\d+ x *)+ *([a-z]+[0-9]*\*?)\]+', 
                line
            )
            for match in matches:
                line = line.replace(
                    match.group(),
                    '_array_{}'.format(match.group(2))
                )
            filtered.append(line)

        return filtered

    @classmethod
    def _line_convert_const_vector(
         cls,
         lines):
        filtered = []
        for line in lines:
            matches = re.finditer(
                r'\< *(.+,)+ .+\>',
                line
            )
            for match in matches:
                line = line.replace(
                    match.group(),
                    '_vector_constant'
                )
            filtered.append(line)

        return filtered

    @classmethod
    def _line_convert_const_array(
         cls,
         lines):
        filtered = []
        for line in lines:
            # Do not consider lines containing the `phi` opcode, which are
            # matched by the regex pattern used here.
            if 'phi' not in line:
                matches = re.finditer(
                    r'\[ *(.+,)+ .+\]', 
                    line
                )
                for match in matches:
                    line.replace(
                        match.group(),
                        '_array_constant'
                    )
            filtered.append(line)

        return filtered

    @classmethod
    def _line_convert_const_float(
         cls,
         lines):
        patterns = [
            r'[\+\-]?[0-9]\.[0-9]+e[\+\-][0-9]{2}',
            r'0x[0-9A-F]+'
        ]
        filtered = []
        for line in lines:
            for pattern in patterns:
                matches = re.finditer(
                    pattern,
                    line
                )
                for match in matches:
                    line = line.replace(
                        match.group(),
                        '_float_constant'
                    )
            filtered.append(line)

        return filtered

    @classmethod
    def _token_remove_symbol(
         cls,
         lines,
         symbol):
        filtered = []
        for line in lines:
            token_sequence = []
            for token in line:
                valid = True
                if token and token[0] != symbol:
                    token_sequence.append(token)
            filtered.append(token_sequence)

        return filtered

    @classmethod
    def _token_convert(
         cls,
         lines):
        patterns = {
            r'\%[\.\_a-zA-Z][\.\_\-a-zA-Z0-9]*': '_named_local',
            r'\%[0-9]+': '_local',
            r'\@[\.\_a-zA-Z][\.\_\-a-zA-Z0-9]*': '_named_global',
            r'\@[0-9]+': '_global',
            r'[a-zA-Z][\.\_0-9a-zA-Z]*:': '_label',
        }
        filtered = []
        for line in lines:
            token_sequence = []
            for token in line:
                replacement = None
                for pattern in patterns:
                    match = re.match(
                        pattern, 
                        token
                    )
                    if match:
                        replacement = patterns[pattern]
                token_sequence.append(replacement or token)
            filtered.append(token_sequence)

        return filtered

    @classmethod
    def _token_convert_complex(
         cls,
         lines):
        filtered = []
        for line in lines:
            token_sequence = []
            for token in line:
                match = re.match(
                    r'(phi|pre|in|preheader|loopexit)(\d+)', 
                    token
                )
                if match is not None:
                    token_sequence.append(match.group(1))
                else:
                    token_sequence.append(token)
            filtered.append(token_sequence)

        return filtered

    @classmethod
    def _token_expand_numbers(
         cls,
         lines):
        filtered = []
        for line in lines:
            token_sequence = []
            for token in line:
                match = re.match(
                    r'[\+\-]?[0-9][0-9]*', 
                    token
                )
                if match is not None:
                    token_sequence.append('_integer_constant')
                else:
                    token_sequence.append(token)
            filtered.append(token_sequence)

        return filtered

    def __init__(
         self,
         **kwargs):
        super().__init__(**kwargs)

    def tokenize(
         self,
         kernel,
         **kwargs):
        '''Tokenize the kernel provided as input.
        '''
        lines = kernel.split('\n')

        # Apply line-oriented pre-tokenization transformations.
        lines = LLVMTokenizer._line_remove_empty_string(lines)
        lines = LLVMTokenizer._line_extract_kernel_body(lines)
        lines = LLVMTokenizer._line_remove_comments(lines)
        lines = LLVMTokenizer._line_remove_multi_spaces(lines)
        lines = LLVMTokenizer._line_convert_type_vector(lines)
        lines = LLVMTokenizer._line_convert_type_array(lines)
        lines = LLVMTokenizer._line_convert_const_vector(lines)
        lines = LLVMTokenizer._line_convert_const_array(lines)
        lines = LLVMTokenizer._line_convert_const_float(lines)
        for symbol in ['(', ')', '[', ']', '{', '}', '<', '>', '=', ',', '*']:
            lines = LLVMTokenizer._line_expand_symbol(
                lines, 
                symbol
            )
        lines = LLVMTokenizer._line_remove_multi_spaces(lines)

        # Apply token-oriented post-tokenization transformations.
        lines = [
            line.split(' ')
            for line in lines
        ]
        for symbol in ['!', '#']:
            lines = LLVMTokenizer._token_remove_symbol(
                lines,
                symbol
            )
        lines = LLVMTokenizer._token_convert(lines)
        lines = LLVMTokenizer._token_convert_complex(lines)
        lines = LLVMTokenizer._token_expand_numbers(lines)

        # Flatten token sequences.
        token_sequence = []
        for line in lines:
            for token in line:
                token_sequence.append(token)

        return token_sequence