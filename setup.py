import pathlib
import re

from setuptools import setup


def parse_package_version():
    package_init_path = pathlib.Path.cwd() / 'deepllvm' / '__init__.py'
    version_pattern = r'__version__\s*=\s*\'([0-9\.]+)\''

    with package_init_path.open(mode='r') as fp:
        version = None
        for line in fp:
            line = line.strip()
            if line and version is None:
                match = re.search(
                    version_pattern,
                    line
                )
                if match is not None:
                    version = match.group(1)

    if version is None:
        raise RuntimeError(
            f'Cannot parse package version from {package_init_path}.'
        )

    return version


setup(
    # Package description.
    name='deepllvm',
    version=parse_package_version(),
    description='A set of tools to analyze LLVM-IR with deep-learning models.',
    packages=['deepllvm'],

    # Package dependencies.
    install_requires=[
        'click==7.1.2',
        'pandas==1.2.2',
        'scikit-learn==0.24.1',
        'torch==1.7.1',
        'tqdm==4.56.0'
    ],
    scripts=['deepllvm-cli'],
)
