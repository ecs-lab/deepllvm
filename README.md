# DeepLLVM - LLVM-IR based source code analysis

DeepLLVM is a deep learning technique to solve the heterogeneous source code mapping problem on sources compiled to LLVM Intermediate Representation. The methodology extracts source code features through a set of convolutional layers and predict the best device a generic kernel should be offloaded with, given information about the context source code will run in.

## Install DeepLLVM
DeepLLVM functionalities are implemented in a handy Python 3 package, which comes with a `setup.py` script that makes installing DeepLLVM straighforward.
The `setup.py` script takes care of retrieving relevant dependencies and making DeepLLVM classes, functions and CLI available to the end user.

The raccomenteded way to use DeepLLVM is to create a Python3 Virtual Environment and to install the package locally using the `setup.py` script.

```
# Step 1 - Clone the DeepLLVM repository from GitLab
git clone https://gitlab.com/ecs-lab/deepllvm.git

# Step 2 - Create and activate a Python3 virtual environment
cd project-deepllvm
python3 -m venv venv
source venv/bin/activate

# Step 3 - Install DeepLLVM locally
pip install .
```

## Use DeepLLVM
DeepLLVM can be used both as a regular Python3 package and as a command line tool.
Once the package is installed as shown in the previous section, all its internal functionalities can be imported in any Python3 script using the usual `import deepllvm` syntax.

However, high-level DeepLLVM functionalities can be accessed via the command line tool `deepllvm-cli` which is shipped along with the package. This enables DeepLLVM model training and testing without going deep into the details of the software architecture of the package.

The following instructions allow you to train and test the CNN model implemented by DeepLLVM using stratified 10-folds cross-validation on the AMD portion of the dataset proposed by Cummins and al. in *"End-to-end Deep Learning of Optimization Heuristics"*.

```
# Step 1 - Extract the AMD dataset
gzip -d data/meta_amd.pd.gz -c > meta_amd.pd

# Step 2 - Generate cross-validation folds
python3 deepllvm-cli            \
    generate-stratified-folds   \
    --n-folds 10                \
    meta_amd.pd                 \
    folds.pickle

# Step 3 - Generate CNN configuration file
python3 deepllvm-cli            \
    dump-config                 \
    cfg_cnn.pickle

# Step 4 - Run stratified 10-folds cross-validation on CPU
python3 deepllvm-cli            \
    evaluate                    \
    --normalize-auxiliaries     \
    --preprocess-auxiliaries    \
    meta_amd.pd                 \
    cfg_cnn.pickle              \
    folds.pickle                \
    results.pth
```

## References
If you use our software, or take inspiration from our pipeline, you can reference our previous research works:

#### (2019) Code Mapping in Heterogeneous Platforms Using Deep Learning and LLVM-IR
```
@inproceedings{barchi2019code,
    author = {Barchi, Francesco and Urgese, Gianvito and Macii, Enrico and Acquaviva, Andrea},
    title = {Code Mapping in Heterogeneous Platforms Using Deep Learning and LLVM-IR},
    year = {2019},
    isbn = {9781450367257},
    publisher = {Association for Computing Machinery},
    address = {New York, NY, USA},
    url = {https://doi.org/10.1145/3316781.3317789},
    doi = {10.1145/3316781.3317789},
    booktitle = {Proceedings of the 56th Annual Design Automation Conference 2019},
    articleno = {170},
    numpages = {6},
    keywords = {Code Mapping, LLVM-IR, Deep Learning, HeterogeneousPlatforms, Embedded Platforms},
    location = {Las Vegas, NV, USA},
    series = {DAC '19}
}
```

#### (2020) Exploration of Convolutional Neural Network models for source code classification
```
@article{barchi2020exploration,
    title = {Exploration of Convolutional Neural Network models for source code classification},
    journal = {Engineering Applications of Artificial Intelligence},
    volume = {97},
    pages = {104075},
    year = {2021},
    issn = {0952-1976},
    doi = {https://doi.org/10.1016/j.engappai.2020.104075},
    url = {https://www.sciencedirect.com/science/article/pii/S0952197620303353},
    author = {Francesco Barchi and Emanuele Parisi and Gianvito Urgese and Elisa Ficarra and Andrea Acquaviva},
    keywords = {Deep learning, LLVM-IR, Code mapping, Heterogeneous platforms},
}
```

#### (2021) Making the Most of Scarce Input Data in Deep Learning-based Source Code Classification for Heterogeneous Device Mapping
```
@article{parisi2021making,
    author={Parisi, Emanuele and Barchi, Francesco and Bartolini, Andrea and Acquaviva, Andrea},
    journal={IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems}, 
    title={Making the Most of Scarce Input Data in Deep Learning-based Source Code Classification for Heterogeneous Device Mapping}, 
    year={2021},
    doi={10.1109/TCAD.2021.3114617}
}
```

## Contacts
For getting in touch with the DeepLLVM development team, open a issue in our GitLab repository, or write an e-mail to `emanuele.parisi@unibo.it` and `francesco.barchi@unibo.it`.
